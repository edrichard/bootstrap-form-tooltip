# bootstrap-form-tooltip

Utilisation de Bootstrap 4.

Deux exemples :
 - Avec une icone 
 - Avec une image SVG 


### Le tooltip Bootstrap

4 positions différentes :

`````html
<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
  Tooltip on top
</button>

<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" title="Tooltip on right">
  Tooltip on right
</button>

<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
  Tooltip on bottom
</button>

<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="left" title="Tooltip on left">
  Tooltip on left
</button>
`````
Avec du HTML dans la balise `title` :

`````html
<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-html="true" title="<em>Tooltip</em> <u>with</u> <b>HTML</b>">
  Tooltip with HTML
</button>
`````
Activer le tooltip : 

`````javascript
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
`````

Ce que génère le plugin :

````html
<!-- Generated markup by the plugin -->
<div class="tooltip bs-tooltip-top" role="tooltip">
  <div class="arrow"></div>
  <div class="tooltip-inner">
    Some tooltip text!
  </div>
</div>
````
